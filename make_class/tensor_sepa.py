# -*- coding: utf-8 -*-

import gensim
import numpy as np
import tensorflow as tf
import tflearn
import tflearn.datasets.mnist as mnist
import numpy as np
from tflearn.data_utils import to_categorical 


def make_model(array, speak, go, carry, find):
    model=gensim.models.KeyedVectors.load_word2vec_format('~/word2vec/word2vec/wiki.bin', binary=True)
    ave_list=[]
    peace_list=[]
    result=[]
    for i in(array):
        for k in(speak, go, carry, find):
            for j in k:
                print(k)
                ave_list.append(model.similarity(i, j))#array:speak...
                print('ave:', ave_list)
            peace_list.append(np.average(ave_list))#peace_listに平均を格納
            print('peace_list:', peace_list)
            ave_list=[]
        result.append(peace_list)#resultに部分リストを格納
        print('result:', result)
        peace_list=[]#peace_list初期化
    return result
                



    
speak=['answer', 'tell', 'say', 'speak', 'talk', 'explain', 'teach']
go=['go', 'come', 'move']
carry=['carry', 'accompany', 'escort', 'deliver', 'bring', 'place', 'navigate', 'follow', 'locate', 'send']
find=['find', 'look', 'meet', 'get', 'pick', 'search']

    
test_list=[]
word_list=[]
check_list=[]


test_speak=['express', 'mouth', 'mention', 'utter']    
test_go=['run', 'transfer', 'shift', 'travel']
test_carry=['bear', 'channel', 'transport', 'transmit', 'submit']
test_find=['seek', 'hunt', 'watch']


word_list=['find']


#model作成
"""
for m in(test_speak, test_go, test_carry, test_find):
    for r in(make_model(m, speak, go, carry, find)):
        test_list.append(r)

print(test_list)
        
check_list=make_model(word_list, speak, go, carry, find)
print(check_list)
"""


test_list=[[0.364648, 0.2701712, 0.39451903, 0.3539716],
           [0.4977854, 0.42662427, 0.47597232, 0.4828534],
           [0.4272017, 0.33620456, 0.43584523, 0.3705163],
           [0.29160538, 0.27371433, 0.26818958, 0.24164514],
           [0.53408724, 0.43023464, 0.48496485, 0.52799654],
           [0.34838167, 0.27318025, 0.42632285, 0.37421072],
           [0.45805153, 0.36530963, 0.47326532, 0.43161508],
           [0.41308624, 0.38042715, 0.44195548, 0.4390241],
           [0.46805325, 0.3745551, 0.4543213, 0.4027606],
           [0.26478463, 0.121612586, 0.26344126, 0.2671829],
           [0.31810865, 0.27442762, 0.43966955, 0.32961953],
           [0.3453501, 0.2841854, 0.3425652, 0.31244323],
           [0.32610574, 0.29934624, 0.3283562, 0.39326134],
           [0.56802124, 0.47853675, 0.45455083, 0.46714005],
           [0.5339501, 0.3938296, 0.54155433, 0.4905778],
           [0.19206071, 0.13768837, 0.17009977, 0.21062607]]

check_list=[[0.24999087, 0.2499573,  0.2500577,  0.24999413]]



#学習設定
ans=[[1,0,0,0],
     [1,0,0,0],
     [1,0,0,0],
     [1,0,0,0],
     [0,1,0,0],
     [0,1,0,0],
     [0,1,0,0],
     [0,1,0,0],
     [0,1,0,0],
     [0,0,1,0],
     [0,0,1,0],
     [0,0,1,0],
     [0,0,0,1],
     [0,0,0,1],
     [0,0,0,1],
     [0,0,0,1]]



#check_list=[[0.4385941, 0.4042512, 0.4273549, 0.42644566]]
ans_list=[[0,0,1,0]] #speak, go, carry, find
  

with tf.Graph().as_default():
    #層の作成
    net = tflearn.input_data(shape=[None, 4])
    net = tflearn.fully_connected(net, 128, activation='ReLU')
    net=tflearn.fully_connected(net, 128, activation='ReLU')
    #net = tflearn.dropout(net, 0.5)

    net =tflearn.fully_connected(net, 4, activation='softmax')
    net = tflearn.regression(net, optimizer='sgd', learning_rate=0.01, loss='categorical_crossentropy')

    #学習の設定

    model = tflearn.DNN(net)
    #model.fit(test_list, ans, n_epoch=2000, show_metric=True)
    #model.save("mod.tfl")
    #テストの設定

    #model.load("model.tfl")

    #モデルのセーブはモデル作成後にmodel.save("ファイル名")
    #モデルのロードは
    #model = tflearn.DNN(net)
    model.load("mod.tfl")

    """
    pred = np.array(model.predict(check_list)).argmax(axis=1)
    print(pred)

    label = ans_list.argmax(axis=1)
    print(label)

    accuracy = np.mean(pred == label, axis=0)
    print(accuracy)
    #saver.save(sess, './model.tflearn')
    """
    #print('score', model.evaluate(check_list, ans_list))
    print('Result')
    print(model.predict(check_list))

 
