#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gensim
import numpy as np
import matplotlib.pyplot as plt


def make_model(array, speak, go, carry, find):
    model=gensim.models.KeyedVectors.load_word2vec_format('~/word2vec/word2vec/wiki.bin', binary=True)
    ave_list=[]
    peace_list=[]
    result=[]
    for i in(array):
        for k in(speak, go, carry, find):
            for j in k:
                ave_list.append(model.similarity(i, j))#array:speak...
            peace_list.append(np.average(ave_list))#peace_listに平均を格納
        result.append(peace_list)#resultに部分リストを格納
        peace_list=[]#peace_list初期化
    return result
                


def separate_sent():
    #test=np.array([['whisper']])
    #test=make_model(test, speak, go, carry, find)
    #test=np.hstack((np.ones((len(test), 1)), test))
        
    
    
    speak=['answer', 'tell', 'say', 'speak', 'talk', 'explain', 'teach']
    go=['go', 'come', 'move']
    carry=['carry', 'accompany', 'escort', 'deliver', 'bring', 'place', 'navigate', 'follow', 'locate', 'send']
    find=['find', 'look', 'meet', 'get', 'pick', 'search']

    
    test_list=[]
    vec_list=[]

    
    test_go=['run', 'transfer', 'shift', 'travel']
    test_carry=['bear', 'channel', 'transport', 'transmit', 'submit']
    test_find=['seek', 'hunt', 'watch']
    test_speak=['express', 'mouth', 'mention', 'utter']


    
    #model作成
    for m in(test_go, test_carry, test_find, test_speak):
        for r in(make_model(m, speak, go, carry, find)):
            test_list.append(r)
            #print(test_list)
    test_list=np.hstack((np.ones((len(test_list), 1)), test_list))#学習用にリストの変形
    test_list=np.array(test_list)
    print(test_list)

    #学習設定
    ans=np.array([1,1,1,1,2,2,2,2,2,3,3,3,4,4,4,4])
    ans=np.reshape(ans, (len(ans), 1))
    weight=np.array=([[0.2, 0.2, 0.2, 0.2, 0.2],
                      [0.3, 0.3, 0.3, 0.3, 0.3],
                      [0.3, 0.3, 0.3, 0.3, 0.3],
                      [0.2, 0.2, 0.2, 0.2, 0.2],
                      [0.3, 0.3, 0.3, 0.3, 0.3],
                      [0.2, 0.2, 0.2, 0.2, 0.2],
                      [0.2, 0.2, 0.2, 0.2, 0.2],
                      [0.3, 0.3, 0.3, 0.3, 0.3],
                      [0.3, 0.3, 0.3, 0.3, 0.3],
                      [0.2, 0.2, 0.2, 0.2, 0.2],
                      [0.3, 0.3, 0.3, 0.3, 0.3],
                      [0.2, 0.2, 0.2, 0.2, 0.2],
                      [0.3, 0.3, 0.3, 0.3, 0.3],
                      [0.3, 0.3, 0.3, 0.3, 0.3],
                      [0.2, 0.2, 0.2, 0.2, 0.2],
                      [0.2, 0.2, 0.2, 0.2, 0.2]])
    weight=np.reshape(weight, (len(weight), 5))
    learnig_rate=0.5
    flag=True
    #学習開始
    plt.plot(test_list)
    plt.show()
    while(flag):
        flag=False
        #print(len(test_list)) #16
        #print(test_list)
        for h in range(len(test_list)):
            #print(test_list)
            model_num=test_list[h,:]#1*5
            #model_num=np.array(model_num) #2*1
            model_num=model_num
            print('model_num:'),
            print(model_num)
            weightT=weight[h,:]
            print('weight.T:'),
            print(weightT.T)
            g=np.dot(weightT.T, model_num)
            print('g:'),
            print(g)
            print('g:{}'.format(g)),
            print('weight:{}'.format(weightT))
            print('\n')
            if(ans[h]==1 and g<0):
                weight[h]=weight[h]+np.dot(learnig_rate, model_num)
                flag=True
            elif(ans[h]==2 and g>0):
                weight[h]=weight[h]-np.dot(learnig_rate, model_num)
                flag=True
            elif(ans[h]==3 and g<0):
                weight[h]=weight[h]+np.dot(learnig_rate, model_num)
                flag=True
            elif(ans[h]==4 and g>0):
                weight[h]=weight[h]-np.dot(learnig_rate, model_num)
                flag=True
                
    print('Result: w0={}, w1={}\n'.format(weightT[0], weightT[1]))
    #print(weight)

    

    

separate_sent()
    
    
