#!/usr/bin/python
# -*- coding: utf-8 -*-
import nltk

# 形態素解析
sentence = raw_input("Please write a sentence:") # 形態素解析をする文字列
sentence = nltk.word_tokenize(sentence)
sentence = nltk.pos_tag(sentence)

print sentence

print '================================='
# 品詞ごとに表示
for s in sentence:
    part = s[1] # 品詞の情報
    if part[0] == "V": # 動詞だけ抜き出す
        print('verb:' + s[0])
    elif part[0] == "N": # 名詞だけ抜き出す
        print('noun:' + s[0])
