#!/usr/bin/python
# -*- coding: utf-8 -*-
# Tell me how many curry there are on the bookcaseの場合のみの対応(いろいろな文章に対応するには文法規則の追加が必要) 
import nltk
from nltk import Tree, ProbabilisticTree

print("=================================================================")
#--- 形態素解析
#sentence_input = raw_input("Please write a sentence:") # 形態素解析をする文字列
sentence_input = "Tell me how many curry there are on the bookcase"
sentence = nltk.word_tokenize(sentence_input)
sentence = nltk.pos_tag(sentence)
print sentence

index = 0
# 品詞が間違っているところを手動で修正(curryが間違えた品詞で出力するため)
for s in sentence:
    if (sentence[index][0] == "how" or sentence[index][0] == "How") and sentence[index + 1][0] == "many":
        word = sentence[index + 2][0]
        sentence[index + 2] = (word, 'NN')
        break
    else:
        index += 1

print('【形態素解析結果】')
print(sentence)
print("=================================================================")

# 辞書型に形態素解析結果を格納
part_dic = {} # キー：単語、値：品詞
for e in sentence:
    part_dic[e[0]] = e[1]

#--- ここから構文解析
in_part = ''
nn_part = ''
vb_part = ''
prp_part = ''
wrb_part = ''
jj_part = ''
ex_part = ''
dt_part = ''

# 文法規則の定義と単語の品詞の割り振り
for s2 in sentence:
    if s2[1] == 'NN':
        if nn_part != '':
            nn_part += ' | \"{}\"'.format(s2[0])
        else:
            nn_part += '\"{}\"'.format(s2[0])
    elif s2[1] == 'IN':
        if in_part != '':
            in_part += ' | \"{}\"'.format(s2[0])
        else:
            in_part += '\"{}\"'.format(s2[0])
    elif 'VB' in s2[1]:
        if vb_part != '':
            vb_part += ' | \"{}\"'.format(s2[0])
        else:
            vb_part += '\"{}\"'.format(s2[0])
    elif s2[1] == 'PRP':
        if prp_part != '':
            prp_part += ' | \"{}\"'.format(s2[0])
        else:
            prp_part += '\"{}\"'.format(s2[0])
    elif s2[1] == 'WRB':
        if wrb_part != '':
            wrb_part += ' | \"{}\"'.format(s2[0])
        else:
            wrb_part += '\"{}\"'.format(s2[0])
    elif s2[1] == 'JJ':
        if jj_part != '':
            jj_part += ' | \"{}\"'.format(s2[0])
        else:
            jj_part += '\"{}\"'.format(s2[0])
    elif s2[1] == 'EX':
        if ex_part != '':
            ex_part += ' | \"{}\"'.format(s2[0])
        else:
            ex_part += '\"{}\"'.format(s2[0])
    elif s2[1] == 'DT':
        if dt_part != '':
            dt_part += ' | \"{}\"'.format(s2[0])
        else:
            dt_part += '\"{}\"'.format(s2[0])

text = """
S -> VP NP
PP -> IN NP
NP -> DT NN | QP NP | EX | PRP | NN AP | PRP NP | VB AP
VP -> VB | VP PP
AP -> NP VP
QP -> WRB JJ
IN -> {}
NN -> {}
VB -> {}
PRP -> {}
WRB -> {}
JJ -> {}
EX -> {}
DT -> {}
"""
text = text.format(in_part,nn_part, vb_part, prp_part, wrb_part, jj_part, ex_part, dt_part)

grammar1 = nltk.CFG.fromstring(text)
parser = nltk.ChartParser(grammar1)

sent = sentence_input.split()
trees = parser.parse(sent) # 入力文章を文法規則に基づいて構文解析する
for tree in trees:
    tree
#--- 構造木の表示
#tree.draw()
s = str(tree) # インスタンスをstring型に変更

print('【構文木】')
print s

print("=================================================================")

part_of_speech = ['S','VP', 'NP', 'PP', 'IN', 'PP', 'DT', 'QP', 'EX', 'PRP', 'NN', 'AP', 'VB', 'WRB', 'JJ'] # 品詞のリスト(今後増やす必要あり)

# 構文木の表示から扱いやすいように変更する
s = s.replace('\n', '')
while 1:
    s = s.replace('  ', ' ')
    if '  ' not in s:
        break

# スペースごとに区切ってリストに入れる
t = s.split(' ')

num = 0 # リストのインデックスをカウント
counter = 0 # 何個目の動詞なのかをカウント
start = 0 # '('をカウント
finish = 0 # ')'をカウント

verbs = [] # 動詞と、動詞とつながっている単語を格納

print ('【動詞句】')
for t2 in t:
    if 'VB' in t2:
        counter += 1
        a = t[num - 1] + ' ' + t2 + ' ' + t[num +1]
        print ('動詞句{}: {}').format(counter, a)

        if '(S' not in t[num - 2]:
            b = t[num -2] + ' ' + a
            for b2 in b:
                if '(' == b2:
                    start += 1
                elif ')' in b:
                    finish += 1

            for t3 in t[num + 2:]:
                b = b + ' ' + t3
                if t3 in '(':
                    start += 1
                if t3 in ')':
                    finish += 1
                if start == finish:
                    break
        else:
            b = a
        verbs.append(b)
    num += 1
print("=================================================================")
print('【動詞の修飾関係】')
counter2 = 0
v2_list = []
for v2 in verbs:
    counter2 += 1
    print('{}: {}').format(counter2, v2)

    v2 = v2.replace('(', '')
    v2 = v2.replace(')', '')

    v2_list.append(v2.split(' '))

#print v2_list

print("=================================================================")

# 家具がある場所を表示する関数
def place_judge(inn):
    # ものの場所リストを読み込み、place_dic辞書に格納
    place_dic = {}
    with open('place_list.csv') as f:
        places = f.readlines()
    for p in places:
        p = p.rstrip('\n')
        p = p.split(';')
        place_dic[p[0]] = p[1]
    if inn in place_dic.keys():
        room = place_dic[inn]
        print('部屋は・・・' + room)
        return room
    else:
        return None

# 品詞を判定して表示する関数
def part_judge(l, dic, num, default=''):
    counter = 0
    syntax = {'how':'many', 'How':'many', 'there':'are', 'There':'are'} # 決まった構文の辞書
    for l2 in l:
        if l2 in dic.keys():
            part = dic[l2]
            if 'VB' in part:
                print('動詞{}:{}').format(num,l2)
            elif part == 'PRP':
                print('人{}:{}').format(num,l2)
            elif part == 'IN':
                print('前置詞{}:{}').format(num,l2)
            elif part == 'WRB':
                s = syntax[l2]
                if (l[counter +1] == s):
                    l2 = l2 + ' ' + s
                print('疑問詞{}:{}').format(num,l2)
            elif part == 'NN':
                print('名詞{}:{}').format(num,l2)
                place_judge(l2)
        counter += 1
    print('=================================================================')


ss = sentence_input.split(' ')

index_counter = 0 # インデックスを数えるカウンター
others = [] # 動詞以外のインデックスを格納

for s in ss:
    if (s not in v2_list[0]) and (s not in v2_list[1]):
        others.append(s)
    index_counter += 1

c = 0 # カウンター
# 結果を表示
for i in v2_list:
    c += 1
    if len(sentence) -1 > c:
        if sentence[c-1][0] not in i: # 動詞が含まれていない句の処理
            part_judge(others, part_dic, c) # part_judge関数呼び出し
            c += 1
    part_judge(i, part_dic,c)




