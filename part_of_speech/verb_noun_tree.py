#!/usr/bin/python
# -*- coding: utf-8 -*-
import treetaggerwrapper as ttw

# 形態素解析
tagger = ttw.TreeTagger(TAGLANG='en',TAGDIR='') # TAGDIRにはじめにダウンロードした4ファイルが置かれているパスを書く
sentence = raw_input("Please write a sentence:") # 形態素解析をする文字列
sentence = sentence.decode('utf-8') # str文字列をunicode文字列に変換
tags = tagger.TagText(sentence)

# 品詞ごとに表示
for text in tags:
    text = text.split('\t')
    part = text[1] # 品詞の情報
    if part[0] == "V": # 動詞だけ抜き出す
        print('verb:' + text[0])
    elif part[0] == "N": # 名詞だけ抜き出す
        print('noun:' + text[0])
