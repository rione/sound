#!/usr/bin/env python
#-*- coding: utf-8 -*-
import sys
import math
import nltk
from nltk.stem.porter import PorterStemmer as PS
import pickle

class Predict:
    def __init__(self, words, word_dict, category_dict):
        self.words = words
        self.word_dict = word_dict
        self.category_dict = category_dict

    # 形態素解析
    def split(self, text):
        ps = PS()
        result = []
        malist = nltk.word_tokenize(text)
        for w in malist:
            w = ps.stem(w) # 語幹抽出
            result.append(w)
        return result

    def score(self, words, category):
        score = math.log(self.category_score(category))
        for word in words:
            score += math.log(self.word_score(word, category))
        return score

    def predict(self, text):
        best_category = None
        max = -sys.maxsize
        words = self.split(text)
        score_list = []
        for category in self.category_dict.keys():
            score = self.score(words, category)
            score_list.append((category, score))
            if score > max:
                max = score
                best_category = category
        return best_category, score_list

    def category_score(self, category):
        sum_categories = sum(self.category_dict.values())
        category_v = self.category_dict[category]
        return float(category_v) / float(sum_categories)

    def word_score(self, word, category):
        n = self.get_word_count(word, category) + 1
        d = sum(self.word_dict[category].values()) + len(self.words)
        return float(n) / float(d)

    def get_word_count(self, word, category):
        if word in self.word_dict[category]:
            return self.word_dict[category][word]
        else:
            return 0

if __name__ == "__main__": 
    with open('words.pickle', mode='rb') as f1:
        words = pickle.load(f1)
    with open('word_dict.pickle', mode='rb') as f2:
        word_dict = pickle.load(f2)
    with open('category_dict.pickle', mode='rb') as f3:
        category_dict = pickle.load(f3)

    p = Predict(words, word_dict, category_dict)

    #--- 予想したい文を入力
    pre, scorelist = p.predict("Meet James at the teepee and follow him")
    print("分類={}").format(pre)
    print(scorelist)
