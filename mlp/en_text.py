"""
##テスト環境###################################################
ubuntu16.04 LTS
Python 3.5.1
conda 4.5.11
#############################################################
"""
#インポート
import numpy as np
import nltk
import os
import glob
import pwd

#英文解析
def analysis(text):
    #改行ごとに分けて解析
    lines = text.split("\n")
    wordsList = []
    tagsList = []
    for line in lines:
        words = nltk.word_tokenize(line)    #文章の配列化
        tags = nltk.pos_tag(words)          #形態素解析
        wordsList.append(words)
        tagsList.append(tags)

    return tagsList

#main
def main():
    result = {} #解析結果格納辞書

    #sample読み込み
    """
    mlp/sample/"カテゴリ名"/*.txt  となるように英文を配置
    英文の書き方はmlp/sample/test/en_text.txtを見てください(ピリオド、!や?はつけない)
    """
    current_dir = os.getcwd()
    for path in glob.glob(current_dir+"/sample/*/*.txt",recursive=True):
        pathName = path.split("/")
        categoty = pathName[-2]             #カテゴリ名
        if (categoty == "test"):        #testファイルは参照しない
            continue
        text = open(path,"r").read()
        #テキスト解析
        tagsList = analysis(text)
        result[str(categoty)] = tagsList    #結果を辞書に追加

    #表示
    keys = [k for k,v in result.items()]
    for key in keys:
        print("<",key,">")
        for sentence in result[key]:
            first,second,third = sentence[0],sentence[1],sentence[2]
            #先頭が主語なら普通文
            if(first[1] == "PRP"):
                print(first,second,third,"--->nomal")

            #先頭が以下の特定用語なら
            elif(first[0] == "Do" or first[0] == "Does" or first[0] == "Did" or first[0] == "Is" or first[0] == "Are" or first[0] == "Was" or first[0] == "Were" or first[0] == "Have" or first[0] == "Has" or first[0] == "Had" or first[0] == "Will" or first[0] == "Would"):
                if(second[1] == "RB"):
                    print(first,second,third,"--->request")
                else:
                    print(first,second,third,"--->yesno")

            #先頭が動詞ならリクエスト疑問文
            elif(first[1] == "VB"):
                print(first,second,third,"--->request")


            else:
                print(first,second,third)

        print("--------------------------------------------------------------------------------------------------------------------\n")


#実行
if __name__ == '__main__':
    main()