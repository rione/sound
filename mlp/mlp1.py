#インポート
from janome.tokenizer import Tokenizer
import os
import glob
import pwd

#Janomeを用いて形態素解析
ja_tokenizer = Tokenizer()

#形態素解析
def ja_tokenize(text):
    res = []
    lines = text.split("\n")

    for line in lines:
        malist = ja_tokenizer.tokenize(line)
        for tok in malist:
            ps = tok.part_of_speech.split(",")[0]
            if not ps in ['名詞','動詞','形容詞']:
                continue

            w = tok.base_form
            if(w == "*" or w == ""):
                w = tok.surface

            if(w == "" or w == "\n"):
                continue

            res.append(w)

        res.append("\n")

    return res

#main
def main():
    #現在のディレクトリを取得
    current_dir = os.getcwd()

    #txtファイルを取得して形態素解析する(target.txtを解析)
    for path in glob.glob(current_dir+"/target.txt",recursive=True):
        path_wakati = "split.txt"

        #split.txtが既にあるなら削除する
        if(os.path.exists(current_dir+"/"+path_wakati)):
            print("find")
            os.remove(current_dir+"/"+path_wakati)

        #形態素解析
        text = open(path,"r").read()
        words = ja_tokenize(text)

        #ファイルへの書き込み
        wt = "/".join(words)
        open(path_wakati,"w",encoding="utf-8").write(wt)

if __name__ == '__main__':
    main()